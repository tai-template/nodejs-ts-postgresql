import dbConnection from './database';
import http, { IncomingMessage, ServerResponse } from 'http';
import express from 'express';
import apiRouter from './routes/api';
import webRouter from './routes/web';
const port = 8182;
const app = express();
/**
 * `app.use(express.json()); ` This line is so important, it can help controllers read the body of a request to fetch data and prevent error: TypeError: Cannot destructure property ... as it is undefined.
 * Source: https://stackoverflow.com/a/64155257
 * Explanation: https://www.educative.io/answers/what-is-the-expressjson-function
 * */ 
app.use(express.json());
app.use(apiRouter);
app.use(webRouter);
app.listen(port, ()=>{
    console.log(`Server is listening on port ${port}`);
});
