import { Request, Response } from "express";
export default class AppController{
    static welcome(req: Request, res: Response)
    {
        let name = req.body.name;
        res.status(200).json("Welcome " + name + " to NodeJS");
    }

}