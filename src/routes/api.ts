import express, {Request, Response} from 'express';
import AppController from '../controllers/AppController';
const router = express.Router();
router.post('/welcome', AppController.welcome);

export default router;